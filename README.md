# jes
Jeff's Elastic Search utility

## Installation
### Prerequisites
[paramiko](https://github.com/paramiko/paramiko)

[requests](http://docs.python-requests.org/en/latest/)

We're not actually using paramiko yet, but have plans to so it's in the setup.py.

    cd jes && sudo python setup.py install

## Configuration
jes.cfg.example should be completed & moved to ~/.jes.cfg.  You might also want to:

    chown 600 ~/.jes.cfg

## Usage
    jes @serveralias command

Currently supported commands are `health`, to get the cluster's overall health and `indices` which will show index health & shard information.
