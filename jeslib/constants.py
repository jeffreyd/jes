#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Jeff's ElasticSearch Utility.
#
# Jeff's ElasticSearch Utility is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeff's ElasticSearch Utility is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeff's ElasticSearch Utility.  If not, see <http://www.gnu.org/licenses/>.
CLUSTER_HEALTH_PATH = "/_cluster/health"
CLUSTER_INDICES_PATH = "/_cluster/state"
CLUSTER_NUMBER_OF_REPLICAS_PATH = "/%(index)s/_settings"

CLUSTER_NUMBER_OF_REPLICAS_DATA = """{
    "index": {
        "number_of_replicas": %(count)d
    }
}"""
