#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Jeff's ElasticSearch Utility.
#
# Jeff's ElasticSearch Utility is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jeff's ElasticSearch Utility is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jeff's ElasticSearch Utility.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function
import json
import time
import paramiko
import requests
import functools
import jeslib.constants
from colorprint import *

DEFAULT_CACHE_TIMEOUT = 3000

class ElasticSearchCluster(object):
    DEFAULT_PORT = 80
    
    def __init__(self, host, port=DEFAULT_PORT, no_https=False, user=None,
            password=None, cache_timeout=DEFAULT_CACHE_TIMEOUT):
        self.host = host
        self.port = int(port)
        self.protocol = 'http' if no_https else 'https'
        self.user = user
        self.password = password
        self.uri = self._build_uri()
        self.cache_timeout = cache_timeout

    def _build_uri(self):
        return "%s://%s:%d/" % (self.protocol, self.host,
                self.port)

    def _build_auth(self):
        if self.user is None or self.password is None:
            return None
        else:
            return (self.user, self.password)
            
    def _request_result(self, path, data=None, verb=None, raw_output=False):
        request_uri = "%s%s" % (self.uri, path)
        auth = self._build_auth()
        if verb is not None and verb is 'get':
            request = requests.get(request_uri, auth=auth, params=data)
        elif verb is not None and verb is 'post':
            request = requests.post(request_uri, auth=auth, data=data)
        elif verb is not None and verb is 'put':
            request = requests.put(request_uri, auth=auth, data=data)
        elif verb is not None:
            raise ValueError("%s is not a valid HTTP verb" % (verb.upper(),))
        elif data is not None:
            request = requests.post(request_uri, auth=auth, data=data)
        else:
            request = requests.get(request_uri, auth=auth)

        return request.text if raw_output else request.json()

    def command(self, command, *args, **kwargs):
        if command == 'health':
            return self.health()
        elif command == 'indices':
            return self.indices()
        else:
            raise NotImplementedError('Command %s has not been implemented' % (command,))

    def health(self):
        response = self._request_result(jeslib.constants.CLUSTER_HEALTH_PATH)
        status = response['status']
        print(status, color=status)
        return status

    def indices(self):
        response = self._request_result(jeslib.constants.CLUSTER_INDICES_PATH)
        indexes = response['metadata']['indices'].keys()
        ret = {}
        for i in indexes:
            response = self._request_result("%s/%s" % (jeslib.constants.CLUSTER_HEALTH_PATH, i))
            ret[i] = response
        longest_key_size = max([len(x) for x in ret])
        for i in ret:
            print("[%s] %s (ActPri: %d, Act: %d, Rel: %d, Init: %d, Unass: %d)" % (response['status'], i.ljust(longest_key_size, ' '),
                    response['active_primary_shards'], response['active_shards'], response['relocating_shards'], 
                    response['initializing_shards'], response['unassigned_shards']), color=response['status'])

        return ret

    def replicacount(self, index, count=1):
        url = jeslib.constants.CLUSTER_NUMBER_OF_REPLICAS_PATH % (index,)
        data = jeslib.constants.CLUSTER_NUMBER_OF_REPLICAS_DATA % (count,)
        response = self._request_result(url, data=data, verb='put')
        res = response.json()
        if res['acknowledged'] == 'true':
            print("Replicas for %s set to %d." % (index, count))

