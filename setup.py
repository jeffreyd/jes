#!env python
# This file is part of Jeff's ElasticSearch Utility.
#
# Jeff's ElasticSearch Utility is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Jeff's ElasticSearch Utility is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Jeff's ElasticSearch Utility.  If not, see <http://www.gnu.org/licenses/>.
import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(name='jes',
    version='1.0',
    description='Jeff\'s Elastic Search utility',
    author='Jeffrey D Johnson',
    author_email='jeffreyd@jeffreyd.org',
    url='https://bitbucket.org/jeffreyd/jes',
    packages=['jeslib', 'colorprint'],
    scripts=['scripts/jes'],
    install_requires=['requests', 'paramiko'],
    keywords='elasticsearch cli',
    long_description=read('README.md'),
    license='GPLv3'
)
